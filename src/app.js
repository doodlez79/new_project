'use strict';

const $ = require("jquery");

window.jQuery = $;

if ($) {
    require('bootstrap');
    require("./js/main.js");
    require("./scss/required.scss");
}